# -*- coding: utf-8 -*-
from odoo import _, api, fields, models

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    full_delivered = fields.Boolean(
        compute='_get_full_delivered',
        store=True,
    )    
    vat = fields.Char(
        related='order_id.partner_id.vat',
    )
    street = fields.Char(
        related='order_id.partner_id.street',
    )
    vendor = fields.Char(
        related='order_id.partner_id.user_id.name',
    )
    delivery_address = fields.Char(
        related='order_id.partner_shipping_id.street',
    )
    delivery_address2 = fields.Char(
        related='order_id.partner_shipping_id.street2',
    )
    pending_qty = fields.Float(
        compute='_get_pending_qty',
        store=True,
    )
    product_name = fields.Char(
        related='product_id.name',
    )
    product_code = fields.Char(
        related='product_id.default_code',
    )
    sale_order = fields.Char(
        related='order_id.name',
    )    

# - Nombre empresa(nombre mostrado)

    @api.depends('product_uom_qty', 'qty_delivered')
    def _get_full_delivered(self):
        for record in self:
            record.full_delivered = record.product_uom_qty == record.qty_delivered
    
    @api.depends('product_uom_qty', 'qty_delivered')
    def _get_pending_qty(self):
        for record in self:
            record.pending_qty = record.product_uom_qty - record.qty_delivered
