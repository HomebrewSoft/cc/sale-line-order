# -*- coding: utf-8 -*-
{
    'name': 'Sale Line Delivery',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'sale'
    ],
    'data': [
        # views
        'views/sale_order_line.xml',
    ],
}
